A HAMMERMAN JEWEL is unique in every way. Old world craftsmanship, ever changing designs and the highest quality precious stones are the hallmarks of every HAMMERMAN Collectible.

Website: https://hammermanjewels.com/
